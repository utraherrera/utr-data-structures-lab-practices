/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.lab.aherrera;

/**
 * Lab pratice to empirically evaluate the growth rate of some sort algorithms.
 * @author Alejandro Herrera
 */
public class Sorting {
    
    /**
     * Sorts the specified array into ascending numerical order using the 
     * bubble sort method.
     * @param array the array to be sorted
     */
    public static void bubbleSort(int[] array) {
        int n = array.length;
        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
    
}
